#include <stdio.h>
#include <limits.h>
#include <stdint.h>

#ifndef PSFPARSE_H
#define PSFPARSE_H

#define PSF2_MAGIC0 0x72
#define PSF2_MAGIC1 0xb5
#define PSF2_MAGIC2	0x4a
#define PSF2_MAGIC3	0x86
#define PSF2_MAGIC_OK(x) ((x)[0]==PSF2_MAGIC0 && (x)[1]==PSF2_MAGIC1 \
&& (x)[2]==PSF2_MAGIC2 && (x)[3]==PSF2_MAGIC3)

typedef struct {
    size_t size;
    unsigned int height, width;
    char bitmap[1024][32];
    char ch[1024];
    float value[1024];
} font;

struct table {
    char ch[256];
    float value[256];
};

font getFont(char* path);
struct table getTable(font f);
void printglyph(char* glyph);

#endif

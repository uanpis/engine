#include <stdio.h>

#ifndef VECTOR_H
#define VECTOR_H

typedef struct {
    int x;
    int y;
} int2;

typedef struct {
    int x;
    int y;
    int z;
} int3;

typedef struct {
    float x;
    float y;
} float2;

typedef struct {
    float x;
    float y;
    float z;
} float3;

typedef struct {
    float3 x;
    float3 y;
    float3 z;
} basis;

typedef struct {
    // points in world space
    float3 a;
    float3 b;
    float3 c;

    // normal of each point
    float3 an;
    float3 bn;
    float3 cn;
} tri;

typedef struct {
    float a;
    float b;
    float c;
    float d;
} plane;

typedef struct {
    size_t size;
    tri *tris;
    int uses_vertex_normals;
} mesh;

int2 _int2(int x, int y);
float2 _float2(float x, float y);
float3 _float3(float x, float y, float z);
float3 scale(float3 a, float k);
float lensq(float3 a);
float len(float3 a);
float3 add(float3 a, float3 b);
float3 subtract(float3 a, float3 b);
float dot(float3 a, float3 b);
float3 cross(float3 a, float3 b);
float3 normalized(float3 a);

basis _basis(float3 x, float3 y, float3 z);
tri _tri(float3 a, float3 b, float3 c);

#endif


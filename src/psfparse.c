#include "psfparse.h"
#include <stdlib.h>
#include <string.h>

const int verbose = 0;

unsigned char magic[4];
unsigned int version;
unsigned int headersize;    /* offset of bitmaps in file */
unsigned int flags;
unsigned int length;	    /* number of glyph_ts */
unsigned int charsize;	    /* number of bytes for each character */
unsigned int height, width; /* max dimensions of glyph_ts */

void printhex(char* string) {
    for (int i = 0; i < 32; i++) {
        unsigned char ch = string[i] & 0xff;
        printf("%x ", ch);
    }
    printf("\n");
}

void printglyph(char* glyph) {
    for (int i = 0; i < height*width; i++) {
        int bitnum = i % 8;
        int bytenum = i / 8;

        char mask = 0x01;
        mask = mask << (width - 1) >> bitnum;

        if (glyph[bytenum] & mask)
            printf("██");
        else
            printf("  ");

        if (i % width == width - 1)
            printf("\n");
    }
}

void copy(char *dest, char *src) {
    for (int i = 0; i < 32; i++) {
        dest[i] = src[i];
    }
}

// get psf2 font
font getFont(char* path) {
    font result = {};

	FILE* fp;
    fp = fopen(path, "r");
    if (fp == NULL) {
        printf("ERROR: could not find file: \"%s\"\n", path);
        fclose(fp);
        exit(0);
    }

    // get magic number and check
    fread(magic, 4, 1, fp);
    if (!PSF2_MAGIC_OK(magic)) {
        printf("ERROR: file format is not pfs2\n");
        fclose(fp);
        exit(0);
    }

    fread(&version,    4, 1, fp);
    fread(&headersize, 4, 1, fp);
    fread(&flags,      4, 1, fp);
    fread(&length,     4, 1, fp);
    fread(&charsize,   4, 1, fp);
    fread(&height,     4, 1, fp);
    fread(&width,      4, 1, fp);

    result.height = height;
    result.width = width;

    if (verbose)
        printf("version:    %u\n"
               "headersize: %u\n"
               "flags:      %u\n"
               "length:     %u\n"
               "charsize:   %u\n"
               "height:     %u\n"
               "width:      %u\n",
               version, headersize, flags, length, charsize, height, width);

    // jump to end of header
    fseek(fp, headersize - 32, 1);

    // copy bitmap information
    for (int j = 0; j < length; j++) {
        fread(&result.bitmap[j], charsize, 1, fp);
    }

    // get chars, ascii only
    char str[1024];
    fgets(str, 1024, fp);
    char delim[2];
    delim[0] = 0xff;

    char* token = strtok(str, delim);
    int i = 0;
    int j = 1;
    result.ch[0] = ' ';
    result.value[0] = 0;
    while (token != NULL) {
        if (strlen(token) <= 1) {
            result.ch[j] = token[0];
            copy(result.bitmap[j], result.bitmap[i]);
            j++;
        }
        token = strtok(NULL, delim);
        i++;
    }
    result.size = j;

    // get brightness for each char
    for (i = 0; i < result.size; i++) {
        char* glyph = result.bitmap[i];
        int count = 0;

        for (j = 1; j < charsize; j++) {
            char thing = glyph[j];
            while (thing != 0x00) {
                count++;
                thing &= thing -1;
            }
        }

        float value = count;
        value /= height * width;
        result.value[i] = value;
    }

    if (verbose) {
        int n = 0;
        printglyph(result.bitmap[n]);
        printf("%c %f\n", result.ch[n], result.value[n]);
    }

    fclose(fp);
    return result;
}

struct table getTable(font f) {
    struct table result;
    for (int i = 0; i < 256; i++) {
        int score = 256;
        for (int chnum = 0; chnum < f.size; chnum++) {
            int newscore = abs((int)(i - 255 * f.value[chnum]));
            if (newscore < score) {
                score = newscore;
                result.ch[i] = f.ch[chnum];
                result.value[i] = f.value[chnum];
            }
        }
    }
    return result;
}


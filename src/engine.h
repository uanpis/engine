#include <ncurses.h>
#include "vector.h"
#include "objparse.h"

#ifndef ENGINE_H
#define ENGINE_H

typedef struct {
    basis basis;
    float3 location;
} cam;

typedef struct {
    int2 screensize;
    float aspect_ratio;
    int size;
    cam camera;
    mesh meshes[100];
} scene;

void setPixelRatio(float r);
void setScreenSize(int x, int y);
void addMesh(mesh m);
cam getCamera();
void setCamera(cam c);
void update();
void drawTri();
void shader();
void initEngine();

#endif

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "engine.h"
#include "psfparse.h"

const float3 ZERO3 = {0, 0, 0};
mesh current_mesh;

// Temporary triangle vars //
float3 point;
float3 cam_point;
float3 ortho_point;

float3 normal; // world space normal
float3 cam_normal; // camera space normal
float3 ortho_normal; // ortho space normal

float3 a;
float3 b;
float3 c;

float3 cam_a; // vertices in camera spaces
float3 cam_b;
float3 cam_c;

// ORTHO space: camera space, but with orthonormal projection
float3 ortho_a;
float3 ortho_b;
float3 ortho_c;

float2 ab; // edge vectors in camera space
float2 bc;
float2 ca;

float3 _ab;
float3 _bc;
float3 _ca;
float2 params;

int clockwise; // clockwise-ness of vertice order from camera

float3 min; // bounds top left
float3 max; // bounds bottom right

int2 MIN; // same but in screen space
int2 MAX;

// plane equation
// ax + by + cz + d = 0
// where (a, b, c) is the (normalized) normal vector,
// x' and y' are coordinates in camera space.
//
// z = -d / (ax' + by' + c)
plane tri_plane;

// triple plane for normals
plane x_plane;
plane y_plane;
plane z_plane;

// Other vars //

float depth[1024][256];

const float zoom = 2.5;
float pixel_ratio = 0.5;
scene the_scene;

int2 screensize;
float aspect_ratio;
cam camera;

// Convert 2D point from camera space to screen space and back //

static int2 cameraToScreen(float2 point) {
    point.x /= aspect_ratio * pixel_ratio;

    point.x *= zoom;
    point.y *= zoom;

    point.x += 1;
    point.y += 1;

    point.x *= 0.5 * screensize.x;
    point.y *= 0.5 * screensize.y;

    int2 result;
    result.x = point.x;
    result.y = point.y;

    return result;
}

static float2 screenToCamera(int2 point) {
    float2 result;
    result.x = point.x;
    result.y = point.y;

    result.x /= 0.5 * screensize.x;
    result.y /= 0.5 * screensize.y;

    result.x -= 1;
    result.y -= 1;

    result.x /= zoom;
    result.y /= zoom;

    result.x *= aspect_ratio * pixel_ratio;

    return result;
}

// transform from world space to camera space
static float3 worldToCamera(float3 point, float3 origin) {
    float3 result;
    float3 v = subtract(point, origin);
    result.z = dot(v, camera.basis.z);
    if (result.z) {
        result.x = dot(v, camera.basis.x) / result.z;
        result.y = dot(v, camera.basis.y) / result.z;
    }
    return result;
}

// world space to ortho space
static float3 worldToOrtho(float3 v, float3 origin) {
    float3 result;
    v = subtract(v, origin);
    result.z = dot(v, camera.basis.z);
    result.x = dot(v, camera.basis.x);
    result.y = dot(v, camera.basis.y);
    return result;
}

// get depth of plane
// point is in CAMERA SPACE
static float getZ(float3 point, plane alpha) {
    float z;
    z = -alpha.d / (alpha.a*point.x + alpha.b*point.y + alpha.c);
    return z;
}

static float3 getOrtho(float3 point, plane alpha) {
    float3 output;
    output.z = getZ(point, alpha);
    output.x = point.x * output.z;
    output.y = point.y * output.z;
    return output;
}

static float getZOrtho(float3 point, plane alpha) {
    float z;
    z = - (alpha.a*point.x + alpha.b*point.y + alpha.d) / alpha.c;
    return z;
}

void setPixelRatio(float r) {
    pixel_ratio = r;
}

void setScreenSize(int x, int y) {
    screensize.x = x;
    screensize.y = y;

    aspect_ratio = x;
    aspect_ratio /= y;
}

void addMesh(mesh m) {
    the_scene.meshes[the_scene.size] = m;
    the_scene.size += 1;
}

cam getCamera() {
    return camera;
}

void setCamera(cam c) {
    camera = c;
}


static int checkTri(tri t, float3 p) {
    float3 ap = subtract(p, cam_a);
    float3 bp = subtract(p, cam_b);

    if ( (fmaf(ap.x, ca.y, -ap.y*ca.x) > 0)
        && (fmaf(bp.x, ab.y, -bp.y*ab.x) > 0)
        && (fmaf(bp.x, bc.y, -bp.y*bc.x) > 0)) {
        return 1;
    } else {
        return 0;
    }
}

int meshnum;
int trinum;
float trirand;

void update() {
    for (int x = 0; x < 1024; x++) {
        for (int y = 0; y < 256; y++) {
            depth[x][y] = 0;
        }
    }
    for (meshnum = 0; meshnum < the_scene.size; meshnum++) {
        current_mesh = the_scene.meshes[meshnum];
        for (trinum = 0; trinum < current_mesh.size; trinum++) {
            tri t = current_mesh.tris[trinum];
            a = t.a;
            b = t.b;
            c = t.c;

            // get vertices in camera space
            cam_a = worldToCamera(a, camera.location);
            cam_b = worldToCamera(b, camera.location);
            cam_c = worldToCamera(c, camera.location);

            // get bounding rectangles (camera space)
            min.x = cam_a.x;
            max.x = cam_a.x;
            if (cam_b.x < min.x) min.x = cam_b.x;
            else max.x = cam_b.x;
            if (cam_c.x < min.x) min.x = cam_c.x;
            else if (cam_c.x > max.x) max.x = cam_c.x;

            min.y = cam_a.y;
            max.y = cam_a.y;
            if (cam_b.y < min.y) min.y = cam_b.y;
            else max.y = cam_b.y;
            if (cam_c.y < min.y) min.y = cam_c.y;
            else if (cam_c.y > max.y) max.y = cam_c.y;

            // exclude triangles that are outside view
            MIN = cameraToScreen(_float2(min.x, min.y));
            MAX = cameraToScreen(_float2(max.x, max.y));
            MAX.x += 1;
            MAX.y += 1;
            if (MAX.x < 0 || MAX.y < 0 || MIN.x > screensize.x || MIN.y > screensize.y)
                continue;

            // face normal (true normal)
            normal = cross(subtract(a, b), subtract(b, c));
            // skip backfaces
            if (dot(normal, subtract(a, camera.location)) > 0) continue;

            srand(trinum);
            trirand = rand() % 65536;
            trirand /= 256;

            // get edge vectors (camera space)
            ab.x = cam_b.x - cam_a.x;
            ab.y = cam_b.y - cam_a.y;

            bc.x = cam_c.x - cam_b.x;
            bc.y = cam_c.y - cam_b.y;

            ca.x = cam_a.x - cam_c.x;
            ca.y = cam_a.y - cam_c.y;

            // get clockwise-ness (camera space)
            // 
            // should always be counter clockwise!

            // more normal stuff
            normal = normalized(normal);

            // ortho a b c
            ortho_a = worldToOrtho(a, camera.location);
            ortho_b = worldToOrtho(b, camera.location);
            ortho_c = worldToOrtho(c, camera.location);

            float3 ba = subtract(ortho_a, ortho_b); // ortho space edges
            float3 cb = subtract(ortho_b, ortho_c);

            ortho_normal = cross(ba, cb);

            // plane equation vars
            tri_plane.a = ortho_normal.x;
            tri_plane.b = ortho_normal.y;
            tri_plane.c = ortho_normal.z;
            tri_plane.d = 
                - tri_plane.a*ortho_a.x
                - tri_plane.b*ortho_a.y
                - tri_plane.c*ortho_a.z;

            // normal planes eqations
            if (current_mesh.uses_vertex_normals) {
                // x
                float3 xa = ortho_a;
                float3 xb = ortho_b;
                float3 xc = ortho_c;
                xa.z = t.an.x;
                xb.z = t.bn.x;
                xc.z = t.cn.x;
                float3 x_plane_normal = cross(subtract(xa, xb), subtract(xb, xc));

                x_plane.a = x_plane_normal.x;
                x_plane.b = x_plane_normal.y;
                x_plane.c = x_plane_normal.z;
                x_plane.d = 
                    - x_plane_normal.x*xa.x
                    - x_plane_normal.y*xa.y
                    - x_plane_normal.z*xa.z;

                // y plane
                float3 ya = ortho_a;
                float3 yb = ortho_b;
                float3 yc = ortho_c;
                ya.z = t.an.y;
                yb.z = t.bn.y;
                yc.z = t.cn.y;
                float3 y_plane_normal = cross(subtract(ya, yb), subtract(yb, yc));

                y_plane.a = y_plane_normal.x;
                y_plane.b = y_plane_normal.y;
                y_plane.c = y_plane_normal.z;
                y_plane.d = 
                    - y_plane_normal.x*ya.x
                    - y_plane_normal.y*ya.y
                    - y_plane_normal.z*ya.z;

                // z
                float3 za = ortho_a;
                float3 zb = ortho_b;
                float3 zc = ortho_c;
                za.z = t.an.z;
                zb.z = t.bn.z;
                zc.z = t.cn.z;
                float3 z_plane_normal = cross(subtract(za, zb), subtract(zb, zc));

                z_plane.a = z_plane_normal.x;
                z_plane.b = z_plane_normal.y;
                z_plane.c = z_plane_normal.z;
                z_plane.d = 
                    - z_plane_normal.x*za.x
                    - z_plane_normal.y*za.y
                    - z_plane_normal.z*za.z;
            }

            _ab = subtract(ortho_b, ortho_a);
            _bc = subtract(ortho_c, ortho_b);
            _ca = subtract(ortho_a, ortho_c);
            _ab = normalized(_ab);
            _bc = normalized(_bc);
            _ca = normalized(_ca);

            // draw pixels!!!
            drawTri(t);
        }
    }
}

int X;
int Y;

void drawTri(tri t) {
    for (X = MIN.x; X < MAX.x; X++) {
        if (X < 0 || X > screensize.x) continue;
        for (Y = MIN.y; Y < MAX.y; Y++) {
            if (Y < 0 || Y > screensize.y) continue;
            float2 temp = screenToCamera(_int2(X, Y));
            point.x = temp.x;
            point.y = temp.y;

            if (checkTri(t, point)) {
                // depth
                float old_z = depth[X][Y];
                ortho_point = getOrtho(point, tri_plane);
                point.z = ortho_point.z;

                if (old_z && point.z > old_z) continue;
                depth[X][Y] = point.z;

                if (current_mesh.uses_vertex_normals) {
                    normal = _float3(getZOrtho(ortho_point, x_plane),
                                     getZOrtho(ortho_point, y_plane),
                                     getZOrtho(ortho_point, z_plane));
                }

                /*if (dot(the_normal, subtract(t.a, camera.location)) > 0) continue;*/

                shader();
            }
        }
    }
}

struct table tab;

void shader() {
    float3 col;
    int VALUE;

    const float bayer[4][4] = {
        { 0,  8,  2,  10 },
        { 12, 4,  14, 6 },
        { 3,  13, 1,  9 },
        { 15, 7,  13, 5 }
    };

    float2 para1;
    float2 para2;
    float2 para3;
    float3 tmp = subtract(ortho_point, ortho_a);

    para1.x = len(cross(tmp, _ab));
    para1.y = dot(tmp, _ab);
    para2.x = len(cross(tmp, _bc));
    para2.y = dot(tmp, _bc);
    para3.x = len(cross(tmp, _ca));
    para3.y = dot(tmp, _ca);

    
    const float3 vectors[7] = {
        { .57735026, .57735026, .57735026 }, // white
        { 1, 0, 0 }, // red
        { 0, 1, 0 }, // green
        { 0, 0, 1 }, // blue
        { .70710678, .70710678, 0 }, // yellow
        { .70710678, 0, .70710678 }, // magenta
        { 0, .70710678, .70710678 }, // cyan
    };

    int scaling = 0.6 * screensize.y / ortho_point.z;
    float3 dither;
    dither.x = - 0.5 + bayer[(int)(trirand + scaling * para1.x) % 4]\
                            [(int)(trirand + scaling * para1.y) % 4] / 16;
    dither.y = - 0.5 + bayer[(int)(trirand + scaling * para2.x) % 4]\
                            [(int)(trirand + scaling * para2.y) % 4] / 16;
    dither.z = - 0.5 + bayer[(int)(trirand + scaling * para3.x) % 4]\
                            [(int)(trirand + scaling * para3.y) % 4] / 16;

    // rgb color
    col.x = 0.5 + dither.x;
    col.y = 0.5 + dither.x;
    col.z = 0.5 + dither.x;
    col = normal;

    if (col.x < 0) col.x = 0;
    if (col.y < 0) col.y = 0;
    if (col.z < 0) col.z = 0;

    const float dither_strength = 1;
    float3 rough_col;
    rough_col.x = col.x * (1 + dither_strength * dither.x);
    rough_col.y = col.y * (1 + dither_strength * dither.y);
    rough_col.z = col.z * (1 + dither_strength * dither.z);

    int biggest = 0;
    float dots[7];
    for (int i = 0; i < 7; i++) {
        dots[i] = dot(rough_col, vectors[i]);
        if (dots[i] > dots[biggest])
            biggest = i;
    }
    if (biggest) {
        attron(COLOR_PAIR(biggest));
    }


    const int k = 2;
    float l = len(col);
    if (l > 0.5) l = 1 - l;
    int diff = 255 * (l - tab.value[(int)(255 * l)]);
    diff = abs(diff);
    VALUE = 255 * len(col) + k * diff * (bayer[X % 4][Y % 4] - 8) / 16;

    if (VALUE > 255) VALUE = 255;
    if (VALUE < 0) VALUE = 0;


    char ch;
    if (VALUE > 127) {
        ch = tab.ch[255 - VALUE];
        attron(A_STANDOUT);
    } else {
        ch = tab.ch[VALUE];
    }

    mvaddch(Y, X, ch);
    attroff(A_STANDOUT);
    attroff(COLOR_PAIR(1));
    attroff(COLOR_PAIR(2));
    attroff(COLOR_PAIR(3));
    attroff(COLOR_PAIR(4));
    attroff(COLOR_PAIR(5));
    attroff(COLOR_PAIR(6));
}

void initEngine() {
    initscr(); // create stdscr
    curs_set(0); // hide cursor
    start_color();
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(3, COLOR_BLUE, COLOR_BLACK);
    init_pair(4, COLOR_YELLOW, COLOR_BLACK);
    init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(6, COLOR_CYAN, COLOR_BLACK);

    cbreak();
    font myfont = getFont("assets/default8x16.psfu");
    tab = getTable(myfont);
    float r = myfont.width;
    r /= myfont.height;
    setPixelRatio(r);
}

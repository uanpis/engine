#include "objparse.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// mode:
// 0 : Z-up
// 1 : Y-up

static float3 swapperoo(float3 vec) {
    float tmp = vec.x;
    vec.x = vec.z;
    vec.z = vec.y;
    vec.y = tmp;
    return vec;
}

mesh getObjMesh(char* path, int mode) {
    mesh result;
    result.size = 0;
    result.uses_vertex_normals = 0;

	FILE* fp;
    char* line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen(path, "r");
    if (fp == NULL) {
        printf("ERROR: could not find file: \"%s\"\n", path);
        return result;
    }

    // count number of faces
    unsigned fsize = 0;
    unsigned vsize = 0;
    unsigned vnsize = 0;
    while ((read = getline(&line, &len, fp)) != -1) {
        char* token = strtok(line, " ");
        if (strcmp(token, "v") == 0)
            vsize++;
        if (strcmp(token, "vn") == 0)
            vnsize++;
        if (strcmp(token, "f") == 0)
            fsize++;
        
    }
    rewind(fp);

    float3 point[vsize];
    float3 normal[vnsize];
    tri face[fsize];
    result.tris = face;


    unsigned pointnum = 0;
    unsigned normal_enabled = 0;
    unsigned normalnum = 0;

    while ((read = getline(&line, &len, fp)) != -1) {
        char* token = strtok(line, " ");

        if (strcmp(token, "v") == 0) {
            token = strtok(NULL, " ");
            point[pointnum].x = atof(token);
            token = strtok(NULL, " ");
            point[pointnum].y = atof(token);
            token = strtok(NULL, " ");
            point[pointnum].z = atof(token);

            switch (mode) {
                case 0:
                    break;
                case 1:
                    point[pointnum] = swapperoo(point[pointnum]);
                    break;
            }

            pointnum++;
        }

        if (strcmp(token, "vn") == 0) {
            normal_enabled = 1;
            result.uses_vertex_normals = 1;

            token = strtok(NULL, " ");
            normal[normalnum].x = atof(token);
            token = strtok(NULL, " ");
            normal[normalnum].y = atof(token);
            token = strtok(NULL, " ");
            normal[normalnum].z = atof(token);

            switch (mode) {
                case 0:
                    break;
                case 1:
                    normal[normalnum] = swapperoo(normal[normalnum]);
                    break;
            }

            normalnum++;
        }

        if (strcmp(token, "f") == 0) {
            char* a = strtok(NULL, " ");
            char* b = strtok(NULL, " ");
            char* c = strtok(NULL, " ");

            int av = atoi(a) - 1;
            int bv = atoi(b) - 1;
            int cv = atoi(c) - 1;

            result.tris[result.size].a = point[av];
            result.tris[result.size].b = point[bv];
            result.tris[result.size].c = point[cv];

            if (normal_enabled) {
                token = strtok(a, "/");
                token = strtok(NULL, "/");
                int an = atoi(token) - 1;

                token = strtok(b, "/");
                token = strtok(NULL, "/");
                int bn = atoi(token) - 1;

                token = strtok(c, "/");
                token = strtok(NULL, "/");
                int cn = atoi(token) - 1;

                result.tris[result.size].an = normal[an];
                result.tris[result.size].bn = normal[bn];
                result.tris[result.size].cn = normal[cn];
            }

            result.size++;
        }
    }

    fclose(fp);
    if (line) free(line);

    /*printf("INFO: loaded file: \"%s\"\n", path);*/
    /*exit(0);*/

    return result;
}

#include <math.h>
#include "vector.h"

int2 _int2(int x, int y) {
    int2 result;
    result.x = x;
    result.y = y;
    return result;
}
float2 _float2(float x, float y) {
    float2 result;
    result.x = x;
    result.y = y;
    return result;
}

float3 _float3(float x, float y, float z) {
    float3 result;
    result.x = x;
    result.y = y;
    result.z = z;
    return result;
}

float3 scale(float3 a, float k) {
    float3 result;
    result.x = a.x * k;
    result.y = a.y * k;
    result.z = a.z * k;
    return result;
}

float lensq(float3 a) {
    float result;
    result = a.x*a.x + a.y*a.y + a.z*a.z;
    return result;
}

float len(float3 a) {
    float result;
    result = lensq(a);
    result = sqrt(result);
    return result;
}

float3 normalized(float3 a) {
    float3 result;
    result = a;
    result = scale(result, 1 / len(result));
    return result;
}

float3 add(float3 a, float3 b) {
    float3 result;
    result.x = a.x + b.x;
    result.y = a.y + b.y;
    result.z = a.z + b.z;
    return result;
}

float3 subtract(float3 a, float3 b) {
    float3 result;
    result.x = a.x - b.x;
    result.y = a.y - b.y;
    result.z = a.z - b.z;
    return result;
}

float dot(float3 a, float3 b) {
    float result;
    result = a.x*b.x + a.y*b.y + a.z*b.z;
    return result;
}

float3 cross(float3 a, float3 b) {
    float3 result;
    result.x = a.y*b.z - a.z*b.y;
    result.y = a.z*b.x - a.x*b.z;
    result.z = a.x*b.y - a.y*b.x;
    return result;
}


tri _tri(float3 a, float3 b, float3 c) {
    tri result;
    result.a = a;
    result.b = b;
    result.c = c;
    return result;
}

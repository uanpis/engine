#include <unistd.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include "engine.h"
#include "vector.h"
#include "psfparse.h"

long int ticks = 0;
int fps = 0;

float3 center;
float distance;

int main() {
    initEngine();

    /*mesh cube = getObjMesh("assets/cube.obj", 0);*/
    /*addMesh(cube);*/

    mesh uanpis = getObjMesh("assets/uanpis.obj", 0);
    addMesh(uanpis);
    center = _float3(0, -1.2, 2);

    /*mesh teapot = getObjMesh("assets/teapot.obj", 1);*/
    /*addMesh(teapot);*/
    /*center = _float3(0, 0, 1);*/

    distance = 10;


    while (1) {
        // clock for determining sleep time after calculations
        clock_t start = clock();

        // get screen size
        float2 screensize;
        getmaxyx(stdscr, screensize.y, screensize.x);
        setScreenSize(screensize.x, screensize.y);

        // clear screen without refreshing
        erase();

        // rotating camera for cube demo
        cam camera = getCamera();

        const float camera_speed = 0.03;
        float angle = -ticks * camera_speed;

        camera.basis.z = normalized(_float3(sin(angle), cos(angle), -0.5));
        camera.location = subtract(center,
            scale(camera.basis.z, distance));

        camera.basis.x = normalized(cross(camera.basis.z, _float3(0, 0, 1)));
        camera.basis.y = cross(camera.basis.z, camera.basis.x);

        setCamera(camera);

        // draw all the triangles
        update();

        move(0, 0);
        printw("%i FPS", fps);
        refresh();

        // calculate fps and sleep for remaining time
        clock_t end = clock();
        fps = CLOCKS_PER_SEC / (end - start);

        int sleep = 16666 - (1000000 * (end - start) / CLOCKS_PER_SEC);
        if ( sleep > 0 ) {
            usleep(sleep);
            fps = 60;
        }

        ticks++;
    }

    endwin();

    return 0;
}
